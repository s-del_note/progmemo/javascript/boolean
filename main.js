"use strict";

(() => {
  let x = Boolean(false);
  if (x) {
    // 実行されない
  }
  console.log(x); // false

  x = new Boolean(false);
  if (x) {
    console.log(x); // [Boolean: false]
    console.log(typeof x); // 'object'
    console.log(x instanceof Boolean); // true
    console.log(x.valueOf()); // false
  }

  let array = [
    false, 0, NaN, '', null, undefined,
    true, 1, 'str', [], {}, Infinity
  ];
  array = array.filter(Boolean);
  console.log(array); // [ true, 1, 'str', [], {}, Infinity ]
})();
