# Boolean の呼び出し方法の注意点

## 関数呼び出しとコンストラクタ呼び出し
`Boolean`[^Boolean] は関数として呼び出した場合と、`new` 演算子を利用してコンストラクタとして呼び出した場合で返り値が異なる。  
関数として呼び出した場合は **プリミティブな `boolean` な値**、コンストラクタで呼び出した場合は **`Boolean` オブジェクト**が返却される。  
例)
```js
let x = Boolean(false); // 関数呼び出し
if (x) { // if (false) と同義
    // 実行されない
}
console.log(x); // false

x = new Boolean(false); // new 演算子を利用したコンストラクタ呼び出し
if (x) { // if (obj) 等と同義
    // 実行される
    console.log(x); // [Boolean: false]
    console.log(typeof x); // 'object'
    console.log(x instanceof Boolean); // true
    console.log(x.valueOf()); // false
}
```
JavaScript において、
```js
false, 0, -0, 0n, "", null, undefined, NaN
```
以外の全ての値は `Truthy`[^Truthy] であるため、`Boolean` オブジェクトは条件式で `true` と判定される。


## 関数呼び出しの利用例
`filter()` のテスト関数に `Boolean` を使用すると、`Truthy` な値のみを取り出して新たな配列を作成できる。
```js
let array = [
  false, 0, NaN, '', null, undefined, // Falsy
  true, 1, 'str', [], {}, Infinity // Truthy
];
array = array.filter(Boolean);
console.log(array); // [ true, 1, 'str', [], {}, Infinity ]
```


## 脚注
[^Boolean]: [Boolean - JavaScript | MDN](https://developer.mozilla.org/ja/docs/Web/JavaScript/Reference/Global_Objects/Boolean)
[^Truthy]: [Truthy - MDN Web Docs 用語集: ウェブ関連用語の定義 | MDN](https://developer.mozilla.org/ja/docs/Glossary/Truthy)
